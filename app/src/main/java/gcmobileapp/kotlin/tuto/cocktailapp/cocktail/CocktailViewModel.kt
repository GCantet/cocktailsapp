package gcmobileapp.kotlin.tuto.cocktailapp.cocktail

import android.app.Application
import androidx.lifecycle.AndroidViewModel

private const val TAG = "CocktailViewModel"

class CocktailViewModel(app: Application) : AndroidViewModel(app){

    private val cocktailRepository = CocktailRepository(app)

    val cocktailsData = cocktailRepository.cocktailsData

    fun findCocktails(filter: String?) {
        if (filter != null) {
            cocktailRepository.getCocktailsFilteredByName(filter)
        } else {
            refreshData()
        }
    }

    fun refreshData() {
        cocktailRepository.refreshData()
    }

}