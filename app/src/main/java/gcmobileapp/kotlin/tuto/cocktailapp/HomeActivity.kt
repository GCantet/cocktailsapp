package gcmobileapp.kotlin.tuto.cocktailapp

import android.content.Context
import android.content.Intent
import android.graphics.ColorFilter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import gcmobileapp.kotlin.tuto.cocktailapp.cocktail.CocktailListActivity

import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.content_home.*

class HomeActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, HomeActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        cocktail_input.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) { }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length != 0) {
                    enableSearchButton()
                } else {
                    disableSearchButton()
                }
            }

        })

        search_button.setOnClickListener {
            CocktailListActivity.startWithFilter(this, cocktail_input.text?.toString()?:"")
        }

        search_all_button.setOnClickListener {
            CocktailListActivity.start(this)
        }

    }

    private fun enableSearchButton() {
        // TODO grey out button (check background selectors)
    }

    private fun disableSearchButton() {
        // TODO grey out button (check background selectors)
    }

}
