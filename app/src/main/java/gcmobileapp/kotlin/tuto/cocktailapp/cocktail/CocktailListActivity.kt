package gcmobileapp.kotlin.tuto.cocktailapp.cocktail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.navigation.NavigationView
import gcmobileapp.kotlin.tuto.cocktailapp.AboutActivity
import gcmobileapp.kotlin.tuto.cocktailapp.HomeActivity
import gcmobileapp.kotlin.tuto.cocktailapp.R
import gcmobileapp.kotlin.tuto.cocktailapp.models.Cocktail
import gcmobileapp.kotlin.tuto.cocktailapp.utils.showSnackbar
import kotlinx.android.synthetic.main.activity_cocktail_list_nav.*
import kotlinx.android.synthetic.main.toolbar.*

class CocktailListActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, CocktailAdapter.CocktailItemListener {

    companion object {
        const val EXTRA_COCKTAIL_FILTER = "cocktail_filter"

        fun start(context: Context) {
            val intent = Intent(context, CocktailListActivity::class.java)
            context.startActivity(intent)
        }

        fun startWithFilter(context: Context, filterValue: String) {
            val intent = Intent(context, CocktailListActivity::class.java)
            intent.putExtra(EXTRA_COCKTAIL_FILTER, filterValue)
            context.startActivity(intent)
        }
    }

    private val drawerLayout by lazy {
        findViewById<DrawerLayout>(R.id.drawer)
    }

    private lateinit var swipeLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewModel: CocktailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cocktail_list_nav)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val filter = intent.getStringExtra(EXTRA_COCKTAIL_FILTER)

        nav_view.setNavigationItemSelectedListener(this)
        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_nav_drawer, R.string.close_nav_drawer)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        viewModel = ViewModelProviders.of(this).get(CocktailViewModel::class.java)
        viewModel.cocktailsData.observe(this, Observer {
            val adapter = CocktailAdapter(this@CocktailListActivity, it, this@CocktailListActivity)
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = adapter
            swipeLayout.isRefreshing = false
        })

        recyclerView = findViewById(R.id.cocktails_recycler_view)
        swipeLayout = findViewById(R.id.swipe_layout)
        swipeLayout.setOnRefreshListener {
            evaluateCocktailList(filter)
        }

        evaluateCocktailList(filter)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_about ->
                AboutActivity.start(this)
            R.id.action_settings -> showSnackbar(drawerLayout, getString(R.string.to_implement))
            R.id.action_cocktails -> start(this)
            R.id.action_favorites -> showSnackbar(drawerLayout, getString(R.string.to_implement))
            R.id.action_home -> HomeActivity.start(this)
            else -> showSnackbar(drawerLayout, getString(R.string.to_implement))
        }
        return true
    }

    private fun evaluateCocktailList(filterValue: String) {
        viewModel.findCocktails(filterValue)
    }

    override fun onCocktailItemClick(cocktail: Cocktail) {
        CocktailDetailActivity.start(this, cocktail)
    }
}
