package gcmobileapp.kotlin.tuto.cocktailapp.cocktail

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import gcmobileapp.kotlin.tuto.cocktailapp.CocktailServiceFactory
import gcmobileapp.kotlin.tuto.cocktailapp.R
import gcmobileapp.kotlin.tuto.cocktailapp.WEB_SERVICE_URL
import gcmobileapp.kotlin.tuto.cocktailapp.models.Cocktail
import gcmobileapp.kotlin.tuto.cocktailapp.models.Drink
import gcmobileapp.kotlin.tuto.cocktailapp.utils.FileHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/* Model / Data Layer from pattern MVVM : Gets data from
    local datasources (database (Room) - json assets/resources)
    remote datasources (web services) */
class CocktailRepository(val app: Application) {

    private val TAG = "CocktailRepository"

    private var cocktailService = CocktailServiceFactory.makeCocktailFactory()

    private val _cocktailsData = MutableLiveData<List<Cocktail>?>()
    val cocktailsData: LiveData<List<Cocktail>?>
        get() = _cocktailsData

    private val listType = Types.newParameterizedType(
        List::class.java, Cocktail::class.java, Drink::class.java
    )

    fun getCocktailDataFromResources() {
        val textFromResources = FileHelper.getTextFromResources(app, R.raw.raw_cocktail)

        val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val adapter: JsonAdapter<List<Cocktail>> = moshi.adapter(listType)
        _cocktailsData.value = adapter.fromJson(textFromResources)?: emptyList()
    }

    fun getCocktailsFilteredByName(cocktailName: String) {
        CoroutineScope(Dispatchers.IO).launch {
            callGetCocktailsFilteredByName(cocktailName)
        }
    }

    fun getAllCocktails() {
        CoroutineScope(Dispatchers.IO).launch {
            callGetAllCocktails()
        }
    }

    @WorkerThread
    private suspend fun callGetCocktailsFilteredByName(cocktailName: String) {
        if (networkAvailable()) {
            val response = cocktailService.getCocktailsFilteredByName(cocktailName).body()
            _cocktailsData.postValue(response?.cocktails)
        }
    }

    @WorkerThread
    private suspend fun callGetAllCocktails() {
        if (networkAvailable()) {
            val serviceData = cocktailService.getAllCocktails().body()
            _cocktailsData.postValue(serviceData?.cocktails)
        }
    }

    fun refreshData() {
        CoroutineScope(Dispatchers.IO).launch {
            callGetAllCocktails()
        }
    }

    @Suppress("DEPRECATION")
    private fun networkAvailable(): Boolean {
        val connectivityManager = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting?:false
    }

}