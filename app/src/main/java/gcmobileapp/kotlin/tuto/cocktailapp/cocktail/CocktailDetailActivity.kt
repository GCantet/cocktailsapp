package gcmobileapp.kotlin.tuto.cocktailapp.cocktail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import gcmobileapp.kotlin.tuto.cocktailapp.R
import gcmobileapp.kotlin.tuto.cocktailapp.models.Cocktail
import kotlinx.android.synthetic.main.activity_cocktail_detail.*
import kotlinx.android.synthetic.main.content_cocktail_detail.*
import kotlinx.android.synthetic.main.toolbar.*

class CocktailDetailActivity : AppCompatActivity() {

    lateinit var cocktail: Cocktail

    companion object {
        const val EXTRA_COCKTAIL = "cocktail"

        fun start(context: Context, cocktail: Cocktail) {
            val intent = Intent(context, CocktailDetailActivity::class.java)
                .putExtra(EXTRA_COCKTAIL, cocktail)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cocktail_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        cocktail = intent.getParcelableExtra(EXTRA_COCKTAIL)

        cocktail_name.text = cocktail.name
        cocktail_image.setImageURI(Uri.parse(cocktail.resId))
        // cocktailImage?.setBackgroundResource(cocktail.resId?:-1)
        // cocktail_image.setImageResource(cocktail.resId?:-1) // TODO Handle errors
    }
}
