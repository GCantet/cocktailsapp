package gcmobileapp.kotlin.tuto.cocktailapp.utils

import gcmobileapp.kotlin.tuto.cocktailapp.R
import gcmobileapp.kotlin.tuto.cocktailapp.models.Cocktail

class CocktailsHelper {
    companion object {
        private const val MARGARITA = "margarita"
        private const val SEX_ON_THE_BEACH = "sex on the beach"
        private const val PINACOLADA = "pinacolada"
        private const val PANAMA = "panama"
        private const val CURACAO_PUNCH = "curacao punch"
        private const val PARADISE = "paradise"

        @JvmStatic
        val cocktails by lazy {
            mutableListOf(Cocktail(0, MARGARITA, "R.drawable.ic_margarita")
                , Cocktail(1, SEX_ON_THE_BEACH, "R.drawable.ic_sex_on_the_beach")
                , Cocktail(2, PINACOLADA, "R.drawable.ic_pinacolada")
                , Cocktail(3, PANAMA, "R.drawable.ic_panama")
                , Cocktail(4, CURACAO_PUNCH, "R.drawable.ic_curacao_punch")
                , Cocktail(5, PARADISE, "R.drawable.ic_paradise"))
        }

        @JvmStatic
        fun findCocktails(cocktailName: String): List<Cocktail> {
            return cocktails.filter { it.name?.contains(cocktailName)?:false }
        }
    }
}