package gcmobileapp.kotlin.tuto.cocktailapp.models

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json

data class Cocktail(
    @Json(name = "idDrink") val id: Int,
    @Json(name = "strDrink") val name: String?,
    @Json(name = "strDrinkThumb") val resId: String?) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readString().toString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(resId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Cocktail> {
        override fun createFromParcel(parcel: Parcel): Cocktail {
            return Cocktail(parcel)
        }

        override fun newArray(size: Int): Array<Cocktail?> {
            return arrayOfNulls(size)
        }
    }
}