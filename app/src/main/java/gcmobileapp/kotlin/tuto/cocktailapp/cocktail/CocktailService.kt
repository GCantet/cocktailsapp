package gcmobileapp.kotlin.tuto.cocktailapp.cocktail

import gcmobileapp.kotlin.tuto.cocktailapp.models.Drink
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CocktailService {

    @GET("filter.php?c=Cocktail")
    suspend fun getAllCocktails(): Response<Drink>

    @GET("search.php")
    suspend fun getCocktailsFilteredByName(@Query("s") name: String): Response<Drink>

}