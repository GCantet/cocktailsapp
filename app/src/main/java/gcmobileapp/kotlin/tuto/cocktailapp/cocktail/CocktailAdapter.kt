package gcmobileapp.kotlin.tuto.cocktailapp.cocktail

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import gcmobileapp.kotlin.tuto.cocktailapp.R
import gcmobileapp.kotlin.tuto.cocktailapp.models.Cocktail

class CocktailAdapter(private val context: Context, private val cocktails: List<Cocktail>?, val itemListener: CocktailItemListener) :
    RecyclerView.Adapter<CocktailAdapter.ViewHolder>() {

    interface CocktailItemListener {
        fun onCocktailItemClick(cocktail: Cocktail)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardView: CardView? = itemView.findViewById(R.id.cardview)
        val cocktailImage: ImageView? = itemView.findViewById(R.id.card_view_cocktail_image)
        val cocktailName: TextView? = itemView.findViewById(R.id.card_view_cocktail_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_cocktail, parent, false)
        return ViewHolder(viewItem)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cocktail = cocktails?.get(position)

        with(holder) {
            cardView?.tag = position
            cardView?.setOnClickListener {
                itemListener.onCocktailItemClick(cocktail!!)
            }
            cocktailName?.text = cocktail?.name
            // cocktailImage?.setImageURI(Uri.parse(cocktail?.resId))
            Glide.with(context)
                .load(cocktail?.resId)
                .into(cocktailImage!!)
            // cocktailImage?.setBackgroundResource(cocktail.resId?:-1)
        }
    }

    override fun getItemCount() = cocktails?.size?:0
}