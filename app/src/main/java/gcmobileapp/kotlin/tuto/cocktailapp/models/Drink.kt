package gcmobileapp.kotlin.tuto.cocktailapp.models

import com.squareup.moshi.Json

data class Drink (@Json(name = "drinks") val cocktails: List<Cocktail>? = emptyList())  {

}
