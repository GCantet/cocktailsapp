package gcmobileapp.kotlin.tuto.cocktailapp

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import gcmobileapp.kotlin.tuto.cocktailapp.cocktail.CocktailService
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class CocktailServiceFactory {

    companion object {
        fun makeCocktailFactory(): CocktailService {
            val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            val retrofit = Retrofit.Builder()
                .baseUrl(WEB_SERVICE_URL)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()
            return retrofit.create(CocktailService::class.java)
        }
    }

}